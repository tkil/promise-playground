import "regenerator-runtime";
import 'promise-polyfill/src/polyfill';
import "regenerator-runtime";

import { fetchXkcd } from "./api/xkcdApi.js";
import { LAST_COMIC_ID } from "./utils/constants";

const getRandomComic = () => {
  return Math.floor(Math.random() * LAST_COMIC_ID + 1);  
}

const main = () => {
  const comicId = getRandomComic();
  fetchXkcd(comicId)
    .then((data) => {
      const comicEl = document.getElementById("comic");
      console.log(data)
      if(comicEl) {
        comicEl.setAttribute("src", data.img);
        comicEl.setAttribute("alt", data.alt);
      }
    })
    .catch((err) => {
      console.error("SOMETHING AWFUL HAPPEN... " + err.message); 
    });
}

window.addEventListener('DOMContentLoaded', main);
