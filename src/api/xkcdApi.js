

//https://xkcd.now.sh?comic=302
const xkcdHost = "https://xkcd.now.sh"

export const fetchXkcd = (comicId) => {
  const result = fetch(`${xkcdHost}?comic=${comicId}`)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.error("ERROR PARSING JSON... " + err.message);
      throw err;
    })
  return result;
}